import { S as r, A as l } from "./Attachment-3d7c9d88.js";
import { F, a as I } from "./Attachment-3d7c9d88.js";
import { openBlock as c, createElementBlock as u } from "vue";
const d = {
  props: ["modelValue", "directory", "type", "maxCount", "readonly", "accept"],
  emits: ["update:modelValue", "change"],
  data() {
    return {
      uiInstance: null
    };
  },
  watch: {
    modelValue: {
      handler(e) {
        var n, o, a, i;
        typeof e == "string" && (this.type === "single" ? (o = (n = this.uiInstance) == null ? void 0 : n.renderDefaultFile) == null || o.call(n, e) : (i = (a = this.uiInstance) == null ? void 0 : a.renderDefaultFile) == null || i.call(a, e.split(",")));
      },
      deep: !0
    }
  },
  mounted() {
    var o, a, i, s;
    const e = this, n = this.$refs.uploadContainer;
    this.type === "single" ? this.uiInstance = new r({
      readonly: e.readonly,
      rootElement: n,
      getFileUploadURL(t) {
        return Promise.resolve(`${e.directory}/${t.uuid}_${t.name}`);
      },
      onNotify(t) {
        e.$emit("update:modelValue", t), e.$emit("change", t);
      }
    }) : e.type === "attachment" ? this.uiInstance = new l({
      readonly: e.readonly,
      accept: e.accept,
      rootElement: n,
      maxCount: e.maxCount,
      getFileUploadURL(t) {
        return Promise.resolve(`${e.directory}/${t.uuid}_${t.name}`);
      },
      onNotify(t) {
        e.$emit("update:modelValue", t), e.$emit("change", t);
      }
    }) : console.warn("不支持该模式:%s,目前仅支持single/attachment", e.type), typeof this.modelValue == "string" && (this.type === "single" ? (a = (o = this.uiInstance).renderDefaultFile) == null || a.call(o, this.modelValue) : (s = (i = this.uiInstance).renderDefaultFile) == null || s.call(i, this.modelValue.split(",")));
  }
}, p = (e, n) => {
  const o = e.__vccOpts || e;
  for (const [a, i] of n)
    o[a] = i;
  return o;
}, m = { ref: "uploadContainer" };
function h(e, n, o, a, i, s) {
  return c(), u("div", m, null, 512);
}
const _ = /* @__PURE__ */ p(d, [["render", h]]);
export {
  F as FileAcceptType,
  I as FileStatus,
  _ as FileUpload
};
