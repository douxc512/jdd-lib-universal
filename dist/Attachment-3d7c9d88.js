function F(i) {
  return i != null;
}
function h() {
  var i = (/* @__PURE__ */ new Date()).getTime(), e = 0;
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(t) {
    var s = Math.random() * 16;
    return i > 0 ? (s = (i + s) % 16 | 0, i = Math.floor(i / 16)) : (s = (e + s) % 16 | 0, e = Math.floor(e / 16)), (t === "x" ? s : s & 3 | 8).toString(16);
  });
}
function g(i) {
  const e = i.split("__").pop();
  return e && decodeURIComponent(e);
}
function L(i) {
  var t;
  const e = (t = i.match(
    /\.(jpg|jpeg|png|bmp|pdf|svg|doc|docx|dps|dpt|csv|flv|gif|mp4|ppt|pptx|tif|tiff|txt|word|wps|wpt|wt|xls|xlsx|xml){1}/gi
  )) == null ? void 0 : t.pop();
  return F(e) ? e.replace(".", "") : "other";
}
const v = {
  401: "登录过期，请重新登录",
  404: "请求资源不存在",
  82024: "sso 认证平台登录过期,请重新登录",
  82023: "sso 认证平台登录过期,请重新登录",
  500: "服务器内部错误"
}, U = {
  "content-type": "application/json; charset=utf-8;"
};
let f = () => {
  setTimeout(() => {
    window.location.replace(`/login?redirect=${encodeURIComponent(window.location.href)}`);
  }, 1e3);
};
function P(i) {
  f = i;
}
async function m(i) {
  const e = document.createElement("div");
  e.id = "jdd-fetch-error-message", e.setAttribute(
    "style",
    "max-width:640px;padding:15px;color:red;border:1px solid #ccc;position:fixed;top:10px;z-index:9999"
  ), e.innerText = i, document.body.append(e), setTimeout(() => {
    e.remove();
  }, 1500);
}
function y(i) {
  if (i.code === 200)
    return Promise.resolve(i.data);
  if (i.code === 82024 || i.code === 82023 || i.result === 401) {
    m(i.message || v[i.code]), f();
    return;
  } else
    return m(i.message || `业务接口错误,Status:${i.status}`), Promise.reject(i);
}
const _ = (i, e = { method: "GET" }) => {
  const t = new Headers({ ...e.headers });
  localStorage.getItem("token") && t.set("authorization", `Bearer ${localStorage.getItem("token")}`);
  let s = i;
  if (e.method === "POST" && typeof e.body == "string")
    t.append("content-type", U["content-type"]);
  else if (!(e != null && e.method) || e.method === "GET") {
    const n = new URLSearchParams();
    e.params && Object.keys(e.params).forEach((l) => {
      const o = e.params[l];
      o != null && n.append(l, o);
    }), n.toString() !== "" && (s = i.indexOf("?") !== -1 ? `${i}&${n.toString()}` : `${i}?${n.toString()}`), delete e.params;
  }
  return fetch(s, {
    ...e,
    headers: t,
    credentials: "include"
  }).then((n) => {
    const { status: l } = n;
    return l === 401 ? (m("登录已过期，请重新登录"), f(), Promise.reject(new Error("登录过期"))) : l === 404 ? (m(`接口返回404,url: ${i}`), Promise.reject(new Error(`接口返回404,url: ${i}`))) : n.clone();
  }).then(async (n) => {
    var l, o;
    if (n.headers.get("content-disposition")) {
      const a = await n.blob(), r = document.createElement("a"), u = window.URL.createObjectURL(a), E = (l = n.headers.get("content-disposition")) == null ? void 0 : l.split(";")[1], p = decodeURIComponent(E).replace(new RegExp("filename=", "g"), "");
      if (p.endsWith("html"))
        return u;
      r.href = u, r.download = p || "file.xlsx", r.click();
      return;
    }
    (o = e.onResponse) == null || o.call(e, n);
    try {
      return y(await n.json());
    } catch (a) {
      console.error(a);
    }
  }).catch((n) => Promise.reject(n));
};
class I {
  constructor(e) {
    this.queue = [], this.executingCount = 0, this.isExecuting = !1, this.config = {}, this.config = {
      OSS_PREFIX: e.OSS_PREFIX,
      maxExecCount: e.maxExecCount ?? 3,
      getFileUploadURL: e.getFileUploadURL,
      onNotify: e.onNotify
    };
  }
  /**
   * 将选中的文件添加到上传队列，同时触发上传任务执行
   * @param files
   */
  addFileToQueue(e) {
    console.info("%c 将%d个文件添加到队列中", "color:gray", e.length), e.forEach((t) => {
      this.queue.push(t);
    }), this.isExecuting ? console.info("%c 当前有任务执行,等待上传完成自动执行", "color:red") : (console.info("%c 当前并无任务执行，开始执行", "color:green"), this.exec());
  }
  /** 执行上传任务，会按照配置同时处理N个任务，直到文件列表全部上传完成 */
  exec() {
    if (this.isExecuting = !0, this.queue.length > 0) {
      console.info(
        "%c 当前有%d个任务等待执行",
        "color:orange",
        this.queue.length
      ), this.executingCount += 1;
      const e = this.queue.shift();
      this.config.onNotify({ ...e, status: d.UPLOADING }), this._uploadFileToOSS(e).then((t) => {
        this.config.onNotify({
          ...e,
          status: d.SUCCESS,
          thumbURL: t
        }), this.executingCount -= 1, this.exec();
      }).catch((t) => {
        this.config.onNotify({ ...e, status: d.FAILED }), this.executingCount -= 1, this.exec();
      }), console.info(
        "%c task pool status: %d/%d (used/total)",
        "color:green",
        this.executingCount,
        this.config.maxExecCount
      ), this.executingCount < this.config.maxExecCount && this.queue.length > 0 && this.exec();
    } else
      console.info("当前没有任务需要执行,或所有任务都执行完毕"), this.isExecuting = !1;
  }
  /**
   * 调用接口获取oss的上传地址，会返回sign等内容
   * @param file
   * @return Promise<string>
   */
  async _getFileUploadUrl(e) {
    try {
      return "getFileUploadURL" in this.config && typeof this.config.getFileUploadURL == "function" ? await this.config.getFileUploadURL(e) : Promise.reject(
        "必须传入获取文件上传地址方法：getFileUploadURL项"
      );
    } catch (t) {
      return console.error(t), Promise.reject(t);
    }
  }
  /**
   * 本地开发环境直接请求PUT到OSS，会提示跨域（oss有配置过限制规则），为了解决跨域同时保证oss访问的安全性，判断如果是本地开发环境，直接走代理
   * @param url 上传地址
   * @returns
   */
  _parseLocalDevUrl(e) {
    let t = e;
    if (window.location.hostname === "localhost") {
      console.log(
        "%c you are now at development environment, replace upload url with local /oss prefix",
        "color:orange"
      );
      const s = t.indexOf(".com") + 4, n = t.substring(0, s);
      t = t.replace(new RegExp(n, "ig"), "/oss");
    }
    return t;
  }
  /** 图片上传到oss方法 */
  async _uploadFileToOSS(e) {
    try {
      const t = await this._getFileUploadUrl(e), { url: s } = await _(
        `/server/getUploadUrl?key=${encodeURIComponent(t)}`
      ), n = await fetch(this._parseLocalDevUrl(s), {
        method: "PUT",
        body: e.originFileObj
      });
      if (n.status === 200) {
        const l = t.split("?")[0];
        return `${this.config.OSS_PREFIX}${l.split(".myqcloud.com").pop()}`;
      } else
        return Promise.reject(`upload failed, response code is ${n.status}`);
    } catch (t) {
      return console.error(t), Promise.reject(t);
    }
  }
}
var d = /* @__PURE__ */ ((i) => (i.DEFAULT = "default", i.UPLOADING = "uploading", i.SUCCESS = "success", i.FAILED = "failed", i))(d || {}), c = /* @__PURE__ */ ((i) => (i.ALL = "*", i.IMAGE = "image/*", i.AUDIO = "audio/*", i.VIDEO = "video/*", i.PDF = ".pdf", i.OFFICE = ".doc,.docx,.xls,.xlsx,.ppt,.pptx", i))(c || {});
class x {
  /**
   * 初始化方法
   * @param {Config} options 配置项
   */
  constructor(e) {
    this.config = {}, this.uploader = null, this.config = {
      readonly: e.readonly ?? !1,
      accept: e.accept ?? "*",
      maxCount: e.maxCount ?? 3,
      maxSize: e.maxSize ?? 1024 * 1024 * 5
    }, this.uploader = new I({
      OSS_PREFIX: e.OSS_PREFIX,
      maxExecCount: e.maxExecCount,
      getFileUploadURL: e.getFileUploadURL,
      onNotify: e.onNotify
    });
  }
  /**
   * 触发文件选择
   * @return
   * ```ts
   * Promise(WrappedFile[])
   * ```
   */
  choose() {
    if (!this.config.readonly)
      return new Promise((e, t) => {
        const s = this._generateInputElement();
        s.addEventListener("change", () => {
          const o = Array.from(s.files).filter((a) => {
            if (this.config.accept === "*")
              return !0;
            if (this.config.accept === ".doc,.docx,.xls,.xlsx,.ppt,.pptx") {
              const r = a.name.lastIndexOf("."), u = a.name.substring(r, a.name.length);
              return ".doc,.docx,.xls,.xlsx,.ppt,.pptx".indexOf(u) !== -1;
            } else
              return new RegExp(this.config.accept).test(a.type);
          }).filter((a) => a.size <= this.config.maxSize);
          o.length > 0 ? (this._mergeSelectedFiles(o), e(o)) : t(new Error("所选文件均不符合过滤条件"));
        }), s.click();
      });
  }
  /**
   * 更新当前已选的文件列表，用于外部删除了文件之后同步到组件内部
   * @access public
   * @param {WrappedFile[]} files 更新之后的完整文件列表
   */
  // updateFiles(files: WrappedFile[]) {
  // this.selectedFiles = files;
  // this.config.onChange?.(files, files);
  // }
  /** 将选择的文件和已选的文件合并 */
  _mergeSelectedFiles(e) {
    const t = this._convertFileToUploadFile(e);
    this.uploader.addFileToQueue(t.slice(0, this.config.maxCount));
  }
  /** 将本地选择的文件转为组件需要的格式 */
  _convertFileToUploadFile(e) {
    return e.map(
      (t) => ({
        uuid: h(),
        status: "default",
        originFileObj: t,
        name: t.name,
        size: t.size,
        type: t.type,
        thumbURL: window.URL.createObjectURL(t)
      })
    );
  }
  /**
   * 根据配置生成对应的input元素，触发文件选择
   * @access private
   * @returns 生成的input元素
   */
  _generateInputElement() {
    const e = document.createElement("input");
    return e.type = "file", e.accept = this.config.accept, e.multiple = this.config.maxCount > 1, e;
  }
}
class $ {
  constructor(e) {
    this.readonly = !1;
    const t = this;
    this.readonly = e.readonly ?? !1, this._idPrefix = h(), this._onNotify = e.onNotify, this.uploadInstance = new x({
      readonly: e.readonly,
      maxCount: 1,
      maxExecCount: 1,
      accept: c.IMAGE,
      OSS_PREFIX: e.OSS_PREFIX || "https://statics.wellyspring.com",
      getFileUploadURL: e.getFileUploadURL,
      onNotify(s) {
        var n;
        t.changedFile = s, t.renderImageByFile(s), (n = e.onNotify) == null || n.call(e, s);
      }
    }), this.render(e.rootElement);
  }
  /** 使用特定的值渲染，用于默认值处理 */
  renderDefaultFile(e) {
    var t;
    if (e && e.length > 0) {
      const s = {
        thumbURL: e,
        name: g(e),
        status: d.SUCCESS,
        originFileObj: null,
        uuid: h()
      };
      this.changedFile = s, this.renderImageByFile(s), (t = this._onNotify) == null || t.call(this, s);
    }
  }
  /**
   * 文件变化了，根据文件状态更新渲染
   * @param file 变化的文件
   */
  renderImageByFile(e) {
    this.fileNameElement.innerText = e.name, this.fileNameElement.setAttribute("title", e.name), this.imageElement.setAttribute("src", e.thumbURL), e.status === d.UPLOADING ? this._handleFileUploading() : e.status === d.FAILED ? this._handleFileUploadFail() : this._handleFileUploadSuccess();
  }
  /** 初始状态，可以点击选择文件 */
  _resetFileUpload() {
    this.selectElement.classList.remove("hide"), this.failElement.classList.add("hide"), this.imageElement.classList.add("hide"), this.uploadMaskElement.classList.add("hide"), this.successElement.classList.add("hide"), this.fileNameElement.innerText = "文件名";
  }
  /** 上传成功，展示图片，同时鼠标移入可以展示浮层，进一步操作预览、删除 */
  _handleFileUploadSuccess() {
    this.selectElement.classList.add("hide"), this.failElement.classList.add("hide"), this.successElement.classList.add("hide"), this.imageElement.classList.remove("hide"), this.uploadMaskElement.classList.add("hide");
  }
  /** 上传失败，提示上传失败，此时点击可以直接再次选择文件 */
  _handleFileUploadFail() {
    this.selectElement.classList.add("hide"), this.imageElement.classList.add("hide"), this.failElement.classList.remove("hide"), this.uploadMaskElement.classList.add("hide"), this.successElement.classList.add("hide");
  }
  /** 文件正在上传中，展示浮层，展示loading效果 */
  _handleFileUploading() {
    this.selectElement.classList.add("hide"), this.imageElement.classList.remove("hide"), this.failElement.classList.add("hide"), this.uploadMaskElement.classList.remove("hide"), this.successElement.classList.add("hide");
  }
  /** 渲染html结构 */
  render(e) {
    const t = `
    <div id="${this._idPrefix}-container" class="jdd-file-upload-container">
      <div id="${this._idPrefix}-preview" class="jdd-file-upload-preview">
        <div id="${this._idPrefix}-select" class="jdd-file-upload-select ${this.readonly ? "hide" : ""}">+</div>
        <img id="${this._idPrefix}-image" src="" class="jdd-file-upload-image hide"></img>
        <div id="${this._idPrefix}-success" class="jdd-file-upload-success hide">
            <div id="${this._idPrefix}-preview-icon" class="jdd-file-preview-icon jdd-icon-eye"></div>
            <div id="${this._idPrefix}-delete-icon" class="jdd-file-delete-icon jdd-icon-trash ${this.readonly ? "hide" : ""}"></div>
        </div>
        <div id="${this._idPrefix}-fail" class="jdd-file-upload-fail hide">
          <div class="jdd-file-upload-fail-icon">!</div>
          <p class="jdd-file-upload-fail-text">上传失败</p>
        </div>
        <div id="${this._idPrefix}-loading-mask" class="jdd-file-upload-mask hide"><span class="icon-shape-half-circle anim-loading"></span></div>
      </div>
      <p id="${this._idPrefix}-name" class="jdd-file-upload-name">文件名</p>
    </div>
  `;
    e.innerHTML = t, this.imageElement = document.getElementById(`${this._idPrefix}-image`), this.selectElement = document.getElementById(`${this._idPrefix}-select`), this.fileNameElement = document.getElementById(`${this._idPrefix}-name`), this.uploadMaskElement = document.getElementById(`${this._idPrefix}-loading-mask`), this.failElement = document.getElementById(`${this._idPrefix}-fail`), this.successElement = document.getElementById(`${this._idPrefix}-success`), this.selectElement.addEventListener("click", () => {
      this.uploadInstance.choose();
    }), this.failElement.addEventListener("click", () => {
      this.uploadInstance.choose();
    }), this.imageElement.addEventListener("mouseenter", () => {
      this.successElement.classList.remove("hide");
    }), document.getElementById(`${this._idPrefix}-preview`).addEventListener("mouseleave", () => {
      this.successElement.classList.add("hide");
    }), document.getElementById(`${this._idPrefix}-preview-icon`).addEventListener("click", (s) => {
      s.preventDefault(), s.stopPropagation(), window.open(this.changedFile.thumbURL);
    }), document.getElementById(`${this._idPrefix}-delete-icon`).addEventListener("click", (s) => {
      var n;
      s.preventDefault(), s.stopPropagation(), this.changedFile = null, this._resetFileUpload(), (n = this._onNotify) == null || n.call(this, null);
    });
  }
}
class w {
  constructor(e) {
    this.allFiles = /* @__PURE__ */ new Map(), this.maxCount = 5, this.readonly = !1;
    const t = this;
    this._idPrefix = h(), this._onNotify = e.onNotify, this.readonly = e.readonly ?? !1, this.maxCount = e.maxCount || 5, this.accept = e.accept || c.ALL, this.uploadInstance = new x({
      readonly: e.readonly,
      maxCount: e.maxCount || 5,
      accept: e.accept || c.ALL,
      OSS_PREFIX: e.OSS_PREFIX || "https://statics.wellyspring.com",
      getFileUploadURL: e.getFileUploadURL,
      onNotify(s) {
        t.allFiles.set(s.uuid, s), t.renderAllFiles();
      }
    }), this.render(e.rootElement);
  }
  /** 根据文件状态生成文件item */
  renderFileItem(e) {
    return `<div class="jdd-file-upload_attachment--item" style="border:${e.status === d.FAILED ? "1px dashed red" : ""}">
    ${e.status !== d.FAILED ? ` <div data-uuid='${e.uuid}' data-action='preview' class='jdd-file-upload_attachment--item-type'>${L(
      e.thumbURL
    )}</div>` : ""}
       
        <div style="flex: 1;width:calc(100% - 45px);padding-left:5px;position: relative;">
          <div class="jdd-file-upload_attachment--item-name" name="${e.name}">${e.name}${e.status === d.FAILED ? '&nbsp;<span class="icon-danger text-danger"style="position: absolute;top:2px"></span>' : ""}</div>
          
          <div class="jdd-file-upload_attachment--item-icons">
          ${e.status === d.UPLOADING ? '<span class="icon-shape-half-circle anim-loading"></span>' : ""}
          
          ${e.status === d.SUCCESS ? '<span class="icon-check-o text-success"></span>' : ""}
          ${e.status !== d.FAILED && !this.readonly ? ` <span data-action="delete" data-uuid="${e.uuid}" class="icon-trash text-danger"></span>` : ""}
          ${e.status === d.FAILED ? `<div> <span style="color:#2491FC">重新上传</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span data-action="delete" data-uuid="${e.uuid}"  style="color:#2491FC">删除</span></div>` : ""}
         
          </div>
        </div>
    </div>`;
  }
  renderAllFiles() {
    var t;
    const e = [];
    this.allFiles.size >= this.maxCount ? this.selectElement.classList.add("hide") : this.selectElement.classList.remove("hide"), this.allFiles.forEach((s) => {
      e.push(this.renderFileItem(s));
    }), this.filesElement.innerHTML = e.join(""), (t = this._onNotify) == null || t.call(this, Array.from(this.allFiles.values()));
  }
  /** 使用特定的值渲染，用于默认值处理 */
  renderDefaultFile(e) {
    const t = this;
    e && e.length > 0 && (e.forEach((s) => {
      const n = {
        thumbURL: s,
        name: g(s),
        status: d.SUCCESS,
        originFileObj: null,
        uuid: h()
      };
      t.allFiles.set(n.uuid, n);
    }), t.renderAllFiles());
  }
  /** 渲染html结构 */
  render(e) {
    const t = this, s = t.readonly, n = `
     <div id="${this._idPrefix}-container_attachment" class="jdd-file-upload-container_attachment">
      <div id="${this._idPrefix}-select" class="jdd-file-upload-select_attachment ${s ? "hide" : ""}">
        <p>+</p>
        <p>点击选择文件</p>
        <p class="jdd-file-upload-select_tip">${this.formatFileAccept()}</p>
      </div>
       <div id="${this._idPrefix}-list" class="jdd-file-list_attachment"></div>
     </div>
   `;
    e.innerHTML = n, this.selectElement = document.getElementById(`${this._idPrefix}-select`), this.filesElement = document.getElementById(`${this._idPrefix}-list`), this.selectElement.addEventListener("click", () => {
      this.uploadInstance.choose();
    }), document.getElementById(`${this._idPrefix}-list`).addEventListener("click", function(l) {
      l.preventDefault(), l.stopPropagation();
      const { uuid: o, action: a } = l.target.dataset || {};
      if (a && a.length > 0 && o) {
        const r = t.allFiles.get(o);
        a === "preview" ? window.open(r.thumbURL) : a === "delete" && (t.allFiles.delete(o), t.renderAllFiles());
      }
    });
  }
  /** 将Accept的文件类型默认展示，方便用户选择时判断
   *
   * accept=.pdf,image/*,*
   */
  formatFileAccept() {
    return `接受${this.accept.split(",").map((t) => t === c.AUDIO ? "音频" : t === c.IMAGE ? "图片" : t === c.VIDEO ? "视频" : t === c.PDF ? "PDF" : t === c.OFFICE ? "Word/Excel/PPT" : t === c.ALL ? "全部" : t).join("、")}类型的文件`;
  }
}
export {
  w as A,
  c as F,
  $ as S,
  d as a,
  _ as f,
  P as s
};
