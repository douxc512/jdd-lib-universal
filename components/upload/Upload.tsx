import { FC, useEffect, useRef } from 'react';
import { SingleImage } from './SingleImage';
import { Attachment } from './Attachment';
import { FileAcceptType, WrappedFile } from './core';

interface FileUploadProps {
  readonly?: boolean;
  accept?: FileAcceptType;
  value?: string;
  onChange?: (v: WrappedFile | WrappedFile[]) => void;
  maxCount?: number;
  /** 上传文件的路径前缀，如: /universal/upload，不需要结尾的/ */
  directory: string;
  type: 'single' | 'attachment';
}

const FileUpload: FC<FileUploadProps> = ({ onChange, value, directory, type, maxCount, readonly, ...rest }) => {
  const reactRef = useRef<HTMLDivElement>();
  const uiInstance = useRef<SingleImage | Attachment>(null);

  useEffect(() => {
    if (uiInstance.current === null) {
      if (type === 'single') {
        uiInstance.current = new SingleImage({
          readonly: readonly ?? false,
          rootElement: reactRef.current,
          getFileUploadURL(file) {
            return Promise.resolve(`${directory}/${file.uuid}_${file.name}`);
          },
          onNotify(file) {
            onChange?.(file);
          },
        });
      } else {
        // attachment mode
        uiInstance.current = new Attachment({
          ...rest,
          readonly: readonly ?? false,
          rootElement: reactRef.current,
          maxCount,
          getFileUploadURL(file) {
            return Promise.resolve(`${directory}/${file.uuid}_${file.name}`);
          },
          onNotify(file) {
            onChange?.(file);
          },
        });
      }
    }
  }, [directory, onChange]);
  useEffect(() => {
    // 只有纯string时才认为是传入的oss地址，需要反向还原到WrappedFile格式然后渲染
    if (typeof value === 'string') {
      if (type === 'attachment') {
        (uiInstance.current as Attachment)?.renderDefaultFile(value.split(','));
      } else {
        (uiInstance.current as SingleImage)?.renderDefaultFile(value);
      }
    }
  }, [value]);
  return <div ref={reactRef} />;
};

export default FileUpload;
