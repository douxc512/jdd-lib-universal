/**
 * 将文件上传到oss
 * 文件会先放到队列中，然后通知上传，上传过程中会更新文件状态并通知到外部
 */

import fetcher from 'components/request';
import { FileStatus } from './core';
import type { WrappedFile } from './core';

/** config */
interface Config {
  /**
   * 文件上传后访问前缀地址
   */
  OSS_PREFIX: string;
  /**
   * 最大同时执行任务数
   * @default 3
   */
  maxExecCount: number;
  /**
   * 上传文件时调用方法获取文件完整存放地址
   * @param File file 待上传的文件
   */
  getFileUploadURL: (file: WrappedFile) => Promise<string>;
  /**
   * 本地文件变化时回调函数
   * @param WrappedFile file 变化的文件
   */
  onNotify: (file: WrappedFile) => void;
}

/**
 * 外部传入的配置项，除maxExecCount外全部需要必填
 */
export type UploaderConfig = Required<Omit<Config, 'maxExecCount'>> & Partial<Pick<Config, 'maxExecCount'>>;
/**
 * 最多同时执行3个文件上传
 * 每次有新的文件添加到队列中时要触发上传动作
 * 队列中所有文件都上传完毕后停止上传
 * 上传过程中要一直更新文件状态
 */
class Uploader {
  /** 待上传的文件列表 */
  private queue: WrappedFile[] = [];
  /** 当前正在执行的任务数 */
  private executingCount = 0;
  /** 当前是否有任务正在执行 */
  private isExecuting = false;
  /** 配置 */
  private config: Config = {} as Config;
  constructor(config: UploaderConfig) {
    this.config = {
      OSS_PREFIX: config.OSS_PREFIX,
      maxExecCount: config.maxExecCount ?? 3,
      getFileUploadURL: config.getFileUploadURL,
      onNotify: config.onNotify,
    };
  }
  /**
   * 将选中的文件添加到上传队列，同时触发上传任务执行
   * @param files
   */
  addFileToQueue(files: WrappedFile[]) {
    console.info('%c 将%d个文件添加到队列中', 'color:gray', files.length);
    files.forEach((file) => {
      this.queue.push(file);
    });
    if (!this.isExecuting) {
      console.info('%c 当前并无任务执行，开始执行', 'color:green');
      this.exec();
    } else {
      console.info('%c 当前有任务执行,等待上传完成自动执行', 'color:red');
    }
  }
  /** 执行上传任务，会按照配置同时处理N个任务，直到文件列表全部上传完成 */
  exec() {
    this.isExecuting = true;
    if (this.queue.length > 0) {
      console.info('%c 当前有%d个任务等待执行', 'color:orange', this.queue.length);
      this.executingCount += 1;
      const task = this.queue.shift();
      this.config.onNotify({ ...task, status: FileStatus.UPLOADING });
      this._uploadFileToOSS(task)
        .then((url) => {
          this.config.onNotify({
            ...task,
            status: FileStatus.SUCCESS,
            thumbURL: window.encodeURIComponent(url),
          });
          this.executingCount -= 1;
          this.exec();
        })
        .catch((error) => {
          this.config.onNotify({ ...task, status: FileStatus.FAILED });
          this.executingCount -= 1;
          this.exec();
        });
      console.info(
        '%c task pool status: %d/%d (used/total)',
        'color:green',
        this.executingCount,
        this.config.maxExecCount,
      );
      // 任务池有空闲，且任务列表有待处理任务，继续
      if (this.executingCount < this.config.maxExecCount && this.queue.length > 0) {
        this.exec();
      }
    } else {
      console.info('当前没有任务需要执行,或所有任务都执行完毕');
      // 没有任务了
      this.isExecuting = false;
    }
  }

  /**
   * 调用接口获取oss的上传地址，会返回sign等内容
   * @param file
   * @return Promise<string>
   */
  private async _getFileUploadUrl(file: WrappedFile) {
    try {
      if ('getFileUploadURL' in this.config && typeof this.config.getFileUploadURL === 'function') {
        // 外部传入了获取上传地址的方法，使用外部的
        return await this.config.getFileUploadURL(file);
      } else {
        return Promise.reject('必须传入获取文件上传地址方法：getFileUploadURL项');
      }
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  }
  /**
   * 本地开发环境直接请求PUT到OSS，会提示跨域（oss有配置过限制规则），为了解决跨域同时保证oss访问的安全性，判断如果是本地开发环境，直接走代理
   * @param url 上传地址
   * @returns
   */
  private _parseLocalDevUrl(url: string) {
    let uploadUrl = url;
    if (window.location.hostname === 'localhost') {
      console.log(
        '%c you are now at development environment, replace upload url with local /oss prefix',
        'color:orange',
      );
      // 开发环境，将上传前缀改为本地
      const length = uploadUrl.indexOf('.com') + 4;
      const replaceTargetStr = uploadUrl.substring(0, length);
      uploadUrl = uploadUrl.replace(new RegExp(replaceTargetStr, 'ig'), '/oss');
    }
    return uploadUrl;
  }
  /** 图片上传到oss方法 */
  private async _uploadFileToOSS(file: WrappedFile): Promise<string> {
    try {
      const uploadUrl = await this._getFileUploadUrl(file);
      const { url } = await fetcher<{ url: string }>(`/server/getUploadUrl?key=${encodeURIComponent(uploadUrl)}`);
      const ret = await fetch(this._parseLocalDevUrl(url), {
        method: 'PUT',
        body: file.originFileObj,
      });
      if (ret.status === 200) {
        const fileSrc = uploadUrl.split('?')[0];
        return `${this.config.OSS_PREFIX}${fileSrc.split('.myqcloud.com').pop()}`;
      } else {
        return Promise.reject(`upload failed, response code is ${ret.status}`);
      }
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  }
}
export default Uploader;
