### 通用的文件上传组件

目标是引入该组件库之后即可集成完整的图片上传功能；支持单图片模式和附件模式两种

---

#### Native JS

index.html

```html
...
<body>
  <div id="root"></div>
  <script type="module" src="./index.ts"></script>
</body>
```

index.ts

```jsx
import 'jdd-lib-universal/dist/fileUpload.css';
import { SingleImageUI } from 'jdd-lib-universal';

new SingleImageUI({
  rootElement: document.getElementById('root'),
  getFileUploadURL(file) {
    return new Promise((resolve, reject) => {
      // 根据需求生成不同的上传地址，包含完整的路径，如: /mes/files/YYYY-MM-DD/xxx
    });
  },
  onNotify(file) {
    // 文件状态变化回调
  },
});
```

#### Use In React

```tsx
import 'jdd-lib-universal/dist/fileUpload.css';
import { useRef } from 'react';
import { SingleImageUI } from 'jdd-lib-universal';
function Upload(props) {
  const elementRef = useRef<HTMLDivElement>();
  useLayoutEffect(() => {
    new SingleImageUI({
      rootElement: elementRef.current,
      getFileUploadURL(file) {
        return new Promise((resolve, reject) => {
          // 根据需求生成不同的上传地址，包含完整的路径，如: /mes/files/YYYY-MM-DD/xxx
        });
      },
      onNotify(file) {
        // 文件状态变化回调
        props.onChange?.(file);
      },
    });
  }, [elementRef, props.onChange]);
  return <div ref={elementRef} />;
}
```

#### Use In Vue

```html
<template>
  <div id="root" />
</template>
<script>
  import 'jdd-lib-universal/dist/fileUpload.css';
  import { SingleImageUI } from 'jdd-lib-universal';
  export default {
    mounted() {
      const _this = this;
      new SingleImageUI({
        rootElement: document.getElementById('root'),
        getFileUploadURL(file) {
          return new Promise((resolve, reject) => {
            // 根据需求生成不同的上传地址，包含完整的路径，如: /mes/files/YYYY-MM-DD/xxx
          });
        },
        onNotify(file) {
          // 文件状态变化回调
          _this.$emit('change', file);
        },
      });
    },
  };
</script>
```
