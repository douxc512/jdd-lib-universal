import { generateUUID } from './utils';
import Uploader from './uploader';
import type { UploaderConfig } from './uploader';
/**
 * 文件选择之后的状态可以根据状态展示不同的UI
 * ```
 * DEFAULT: 默认状态，选择之后即该状态
 * UPLOADING: 上传中
 * SUCCESS: 上传成功
 * FAILED: 上传失败
 * ```
 */
export enum FileStatus {
  DEFAULT = 'default',
  UPLOADING = 'uploading',
  SUCCESS = 'success',
  FAILED = 'failed',
}

/**
 * 可选的文件类型
 * ```ts
 * ALL - 所有文件
 * IMAGE - 仅图片文件
 * AUDIO - 仅音频文件
 * VIDEO - 仅视频文件
 * OFFICE - 仅Office文件
 * ```
 */
export enum FileAcceptType {
  ALL = '*',
  IMAGE = 'image/*',
  AUDIO = 'audio/*',
  VIDEO = 'video/*',
  PDF = '.pdf',
  OFFICE = '.doc,.docx,.xls,.xlsx,.ppt,.pptx',
}
/**
 * 文件上传的基础配置项
 */
interface CoreConfig {
  /** 只读模式，不能操作，只用于展示;default:false */
  readonly?: boolean;
  /**
   * 文件选择时默认展示的文件类型
   * @default ALL
   */
  accept?: FileAcceptType;
  /**
   * 最多允许选择几个文件
   * @default 1
   */
  maxCount?: number;
  /**
   * 每个文件的大小限制
   * @default 5MB
   */
  maxSize?: number;
}
export type Config = CoreConfig & UploaderConfig;
/** 本地选择的文件，二次包装 */
export interface WrappedFile extends Partial<File> {
  /** 文件唯一的id，自动生成 */
  uuid: string;
  /** 文件的预览地址，如果是本地选择，预览地址使用URL.createObjectURL生成，如果是oss外部地址，直接是具体的访问链接 */
  thumbURL: string;
  /** 状态 */
  status: FileStatus;
  /** 本地选择时，存在originFileObj */
  originFileObj?: File | null;
}
/**
 * 文件上传核心功能
 * ```code
 * 1. 选择文件
 * 2. 配置文件类型、大小、过滤规则、如何获取上传链接、生成唯一uuid等
 * 3. 上传到oss，并将实际的访问地址返回
 * 4. 文件状态变化时通知外部调用方
 * ```
 */
class FileUploadCore {
  /** 默认配置项 */
  private config: CoreConfig = {};

  // /** 本地维护的文件列表 */
  // private selectedFiles: WrappedFile[] = [];
  private uploader: Uploader | null = null;
  /**
   * 初始化方法
   * @param {Config} options 配置项
   */
  constructor(options: Config) {
    this.config = {
      readonly: options.readonly ?? false,
      accept: options.accept ?? FileAcceptType.ALL,
      maxCount: options.maxCount ?? 3,
      maxSize: options.maxSize ?? 1024 * 1024 * 5,
    };
    this.uploader = new Uploader({
      OSS_PREFIX: options.OSS_PREFIX,
      maxExecCount: options.maxExecCount,
      getFileUploadURL: options.getFileUploadURL,
      onNotify: options.onNotify,
    });
  }
  /**
   * 触发文件选择
   * @return
   * ```ts
   * Promise(WrappedFile[])
   * ```
   */
  choose() {
    if (this.config.readonly) {
      return;
    }
    return new Promise<File[]>((resolve, reject) => {
      const inputEle = this._generateInputElement();

      inputEle.addEventListener('change', () => {
        // input 中的files只是类似数组的fileList，先转成标准数组再处理
        const files = Array.from(inputEle.files);
        // accept只是建议性要求，并非强制要求；mdn文档中有提到该部分逻辑
        // https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/accept
        // 此处需要手动过滤一次
        const suffixFilteredFiles = files.filter((file) => {
          if (this.config.accept === FileAcceptType.ALL) {
            return true;
          } else if (this.config.accept === FileAcceptType.OFFICE) {
            // Office的文件通过判断文件名处理
            const lengthToLastDot = file.name.lastIndexOf('.');
            const fileSuffix = file.name.substring(lengthToLastDot, file.name.length);
            return FileAcceptType.OFFICE.indexOf(fileSuffix) !== -1;
          } else {
            // 其他的文件直接通过MIME类型正则匹配
            return new RegExp(this.config.accept).test(file.type);
          }
        });
        // 文件类型过滤完成，过滤文件大小
        const sizeFilteredFiles = suffixFilteredFiles.filter((file) => {
          return file.size <= this.config.maxSize;
        });
        if (sizeFilteredFiles.length > 0) {
          this._mergeSelectedFiles(sizeFilteredFiles);
          resolve(sizeFilteredFiles);
        } else {
          reject(new Error('所选文件均不符合过滤条件'));
        }
      });
      inputEle.click();
    });
  }

  /**
   * 更新当前已选的文件列表，用于外部删除了文件之后同步到组件内部
   * @access public
   * @param {WrappedFile[]} files 更新之后的完整文件列表
   */
  // updateFiles(files: WrappedFile[]) {
  // this.selectedFiles = files;
  // this.config.onChange?.(files, files);
  // }
  /** 将选择的文件和已选的文件合并 */
  private _mergeSelectedFiles(files: File[]) {
    const changedFiles = this._convertFileToUploadFile(files);
    // this.selectedFiles = changedFiles.concat(this.selectedFiles);
    // // 文件类型过滤完成，需要再次过滤maxCount，超过的部分直接移除
    // this.selectedFiles = changedFiles.slice(0, this.config.maxCount);
    this.uploader.addFileToQueue(changedFiles.slice(0, this.config.maxCount));
  }
  /** 将本地选择的文件转为组件需要的格式 */
  private _convertFileToUploadFile(files: File[]) {
    return files.map(
      (file) =>
        ({
          uuid: generateUUID(),
          status: FileStatus.DEFAULT,
          originFileObj: file,
          name: file.name,
          size: file.size,
          type: file.type,
          thumbURL: window.URL.createObjectURL(file),
        } as WrappedFile),
    );
  }
  /**
   * 根据配置生成对应的input元素，触发文件选择
   * @access private
   * @returns 生成的input元素
   */
  private _generateInputElement() {
    const inputEle = document.createElement('input');
    inputEle.type = 'file';
    inputEle.accept = this.config.accept;
    inputEle.multiple = this.config.maxCount > 1;
    return inputEle;
  }
}

export default FileUploadCore;
