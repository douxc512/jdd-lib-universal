/** 判断值不为null或者undefined */
export function valueNotNullAndUndefined(val: any) {
  return val !== null && val !== undefined;
}
/** 简单的对当前日期做格式化处理：YYYY-MM-DD */
export function getDate() {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  return `${year}-${month}-${day}`;
}
/** 按照固定格式生成一个UUID，用于上传文件时添加到文件名前部以避免文件名冲突 */
export function generateUUID() {
  // Public Domain/MIT
  var d = new Date().getTime(); //Timestamp
  var d2 = 0; //Time in microseconds since page-load or 0 if unsupported
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16; //random number between 0 and 16
    if (d > 0) {
      //Use timestamp until depleted
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      //Use microseconds since page-load if supported
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
}

/**
 * 从地址中获取文件名和后缀
 * @param url 地址
 */
export function getFileNameFromUrl(url: string) {
  const name = url.split('__').pop();
  return name ? decodeURIComponent(name) : name;
}

/**
 * 从地址或者文件名中获取后缀
 * @param url 文件地址
 * @returns 后缀
 */
export function getFileSuffixFromUrl(url: string) {
  const suffix = url
    .match(
      /\.(jpg|jpeg|png|bmp|pdf|svg|doc|docx|dps|dpt|csv|flv|gif|mp4|ppt|pptx|tif|tiff|txt|word|wps|wpt|wt|xls|xlsx|xml){1}/gi
    )
    ?.pop();
  if (valueNotNullAndUndefined(suffix)) {
    return suffix!.replace('.', '');
  } else {
    return 'other';
  }
}

/**
 * 判断给定的文件是否符合accept的要求；判断文件类型和文件后缀
 * @param file
 * @param accept
 */
export function isFileMatchAccept(file: File, accept: string) {
  if (accept === '*') {
    // 允许所有文件
    return true;
  }
  if (accept.indexOf('image/*') !== -1) {
    // accept 允许了所有图片，判断文件类型如果是图片就直接允许，否则走通配逻辑
    if (file?.type.indexOf('image/') !== -1) {
      return true;
    }
  }
  // 没有指定通配符，则文件后缀必须强校验；如果没有文件名，则认为不符合要求
  return (
    valueNotNullAndUndefined(file?.name) &&
    accept.indexOf(getFileSuffixFromUrl(file?.name!)) !== -1
  );
}
