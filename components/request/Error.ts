/** 错误code码处理 */
export const ErrorMessageMap: Record<string | number, string> = {
  401: '登录过期，请重新登录',
  404: '请求资源不存在',
  82024: 'sso 认证平台登录过期,请重新登录',
  82023: 'sso 认证平台登录过期,请重新登录',
  500: '服务器内部错误',
};
