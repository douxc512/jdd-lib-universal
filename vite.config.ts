// vite.config.js
import { resolve } from 'path';
import { defineConfig } from 'vite';
import React from '@vitejs/plugin-react';
import Vue from '@vitejs/plugin-vue';

export default defineConfig({
  optimizeDeps: {
    entries: ['example/index.html'],
  },
  resolve: {
    alias: {
      components: resolve(__dirname, 'components'),
    },
  },
  plugins: [React(), Vue()],
  build: {
    outDir: 'dist',
    lib: {
      formats: ['es'],
      // Could also be a dictionary or array of multiple entry points
      entry: {
        index: 'components/index.ts',
        'index.vue': 'components/index.vue.ts',
      },
    },
    rollupOptions: {
      cache: true,
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue', 'react'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
});
