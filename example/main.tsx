import '../dist/style.css';
import { FileUpload } from '../dist';
import React from 'react';
import { createRoot } from 'react-dom/client';

const root = createRoot(document.getElementById('app')!);

root.render(
  <>
    <FileUpload
      key='singleImage'
      value='https://statics.wellyspring.com/universal/files/d3e6f579-04d0-488f-aac6-ed542fcb987a_DTVjVsuXcAAeuGf.jpg'
      type='attachment'
      directory={'/universal/test'}
      onChange={(file) => {
        console.log(file);
      }}
    />
    <FileUpload
      key='attachment'
      value='https://statics.wellyspring.com/universal/files/d3e6f579-04d0-488f-aac6-ed542fcb987a_DTVjVsuXcAAeuGf.jpg'
      type='single'
      directory={'/universal/test'}
      onChange={(file) => {
        console.log(file);
      }}
    />
  </>
);
